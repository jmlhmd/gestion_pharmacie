package com.formation.integcontinue.hello_world_app;

public class User{

    private String nom;
    private String prenom;


    public User(String nom, String prenom) {
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    public String getNom(){ return nom;}

    public String getPrenom(){ return prenom;}

    public void setNom(String nom){ this.nom = nom;}

    public void setPrenom(String prenom){ this.prenom = prenom;}
}